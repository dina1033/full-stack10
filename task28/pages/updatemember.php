<?php
include 'app/config/dbconnection.php';
global $dbh;
$id = $_GET["id"];
$stmt = $dbh->prepare("select * from user where id=$id");
$stmt->execute(array($id));
$rows = $stmt->fetch(PDO::FETCH_OBJ);

if (isset($_POST['fname']) && isset($_POST['email']) && isset($_POST['lname']) && isset($_POST['password'])) {
$fname = $_POST["fname"];
$lname = $_POST["lname"];
$email = $_POST["email"];
$password = $_POST["password"];
$hash = sha1($password);
$stmt = $dbh->prepare("update  user  set fname='$fname', lname='$lname',password='$hash' , email='$email' where id=$id");
$stmt->execute(array($id));
$count = $stmt->rowcount();
if ($count > 0) {
    header("Location: ?p=viewdata");
    exit();
}
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="puplic/css/bootstrap.min.css">
        <link rel="stylesheet" href="puplic/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>
    <body>
        <div class="container mt-5">
            <div class="card bg-light mt-5">
                <article class="card-body mx-auto" style="max-width: 400px;">
                    <h4 class="card-title mt-3 mb-5 text-center">Update Account</h4>
                    <form method="post">
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                            </div>
                            <input name="fname" class="form-control" placeholder="first name" type="text"
                                   value="<?php echo " $rows->fname"; ?>">
                        </div>  
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                            </div>
                            <input name="lname" class="form-control" placeholder="last name" type="text" 
                                   value="<?php echo" $rows->lname"; ?>">
                        </div>  
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                            </div>
                            <input name="email" class="form-control" placeholder="Email address" type="email"
                                   value="<?php echo " $rows->email" ; ?>">
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <input name="password" class="form-control" placeholder="Create password" type="password" 
                                   value="<?php echo "$rows->password "; ?>">
                        </div>  

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">update  </button>
                        </div>         

                    </form>
                </article>
            </div>  

        </div> 




    </body>
</html>
