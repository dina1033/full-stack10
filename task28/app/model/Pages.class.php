<?php

class Page
{
    public $pageName;

    public function register()
    {
        require_once 'pages/register.php';
    }

    public function viewdata()
    {
        require_once 'pages/viewdata.php';
    }
    public function deletemember()
    {
        require_once 'pages/deletemember.php';
    }
    public function updatemember()
    {
        require_once 'pages/updatemember.php';
    }

    public function __call($name, $arguments)
    {
        require_once 'pages/404.view.php';
    }
}