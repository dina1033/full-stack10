$(document).ready(function () {

    $('.open').click(function () {
        $('.sidebar').addClass('active');
        $(this).hide();
        $('.get img').hide();
    });

    $('.Close').click(function () {
        $('.sidebar').removeClass('active');
        $('.open').show();
        $('.get img').show();

    });

    //   toolbar fillter
    $('.toolbar-btn').click(function () {
        var p = $(this).data('role');
        if (p === 'offer') {
            $('.p-cont > div').show()
        } else if (p === 'dates') {
             $('.p-cont > div').show()
        } else {
            
        } {
            $('.p-cont > div').hide()
            $('.p-cont').contents().filter('#' + p).fadeIn()
        }
    })
    // wow.js
    new WOW().init();

    // change portifolio btns bg (portifolio section)
    $(".btns button").click(function () {
        $(this).addClass("active-btn").siblings().removeClass("active-btn");
        var fill = $(this).data("role");
        if (fill === 'all') {
            $('.imgs > div').fadeIn();
        }
        else {
            $('.imgs > div').fadeOut();
            $('.imgs').contents().filter("#" + fill).fadeIn();
        }
    });
    ////////////////////////////////
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('.nav').addClass('affix');
            
        } else {
            $('.nav').removeClass('affix');
        }
    });

    //////////////scrolltop
    $(window).scroll(function(){

        if($(this).scrollTop() > 100 ){
            $('#scrolltop').fadeIn();
        }
           
        else{
            $('#scrolltop').fadeOut();
        }


    });
    $('#scrolltop').on('click', function(){
        $('html,body').animate({
            scrollTop : 0
        },500);
    })


    ////////////////////////////////////////////////

    $('.navTrigger').click(function () {
        $(this).toggleClass('active');
        $("#mainListDiv").toggleClass("show_list");
        $("#mainListDiv").fadeIn();
    
    });
    ////////////////////////
    

    /////////loading
    $(window).on('load', function () {
        $(".loading-overlay").fadeOut(4000);
        $(".lds-heart").fadeOut(4000);
        $(".lds").fadeOut(4000);
        $("body").css("overflow", "auto");
    });

});

var TheOverlay = document.getElementById("my-overlay");
function OpenDiv(){
    TheOverlay.style.transform ="scale(1)"
}
function CloseDiv(){
    TheOverlay.style.transform ="scale(0)"
}






function show() {
    var p = document.getElementById('pwd');
    p.setAttribute('type', 'text');
}

function hide() {
    var p = document.getElementById('pwd');
    p.setAttribute('type', 'password');
}

