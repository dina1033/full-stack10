import React from 'react';

const Contact = () => (
    <section className="contact">
        <div className="container">
            <h1>Contact Us</h1>
            <p className="mb-5 mt-2">for any inquriries message us</p>
            <div className="row">
                <div className="input col">
                    <input className="form-control " type="text" placeholder="Enter your Name"></input>
                    <input className="form-control mt-4 " type="text" placeholder="Enter your Email"></input>
                    <input className="form-control mt-4" type="text" placeholder="Enter your Phone"></input>
                </div>
                <div className="input2 col">
                    <textarea className="form-control" type="text" placeholder="Enter your Message" rows="6.9"></textarea>
                </div>
            </div>
            <div className="clearfix"></div>
            <button className="btn2">Click Here</button>
        </div>
    </section>
);
export default Contact;