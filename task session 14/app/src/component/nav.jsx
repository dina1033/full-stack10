import React from 'react';

const Header = () => (
    <header>
        <div className="container">
            <div><a href="#">Brand</a></div>

            <nav>
                <ul className="nav">
                    <li className="nav-item">
                        <a className="nav-link active" href="#">Home</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">About</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Contact</a>
                    </li>

                </ul>
            </nav>
        </div>
    </header>
);
export default Header;