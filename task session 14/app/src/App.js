import React, { Fragment } from 'react';
import Header from './component/nav';
import About from './component/about';
import Contact from './component/contact';
import Footer from './component/footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


const App =()=> (

    <Fragment>
      <Header/>
      <About/>
      <Contact/>
      <Footer/>
      </Fragment>
  );


export default App;
