<?php

require_once 'shape.class.php';

class circle extends shape {

    const shape_type = 3;

    protected $radius;
    private $id;
    public $name;

    function circle($r) {
        parent::shape(1, 2);
        $this->id = uniqid();
        $this->radius = $r;
    }

    function circlearea() {
        $area = 3.14 * $this->radius * $this->radius;
        return $area;
    }

    function getname() {
        parent::getname();
    }

    function getFullDescription() {
        echo 'Shape<' . $this->id . '>: ' . $this->name . ' - ' . $this->radius;
    }

}


