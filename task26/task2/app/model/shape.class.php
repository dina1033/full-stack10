<?php

class shape {
    const shape_type = 1;

    public $name;
    protected $length;
    protected $width;
    private $id;

    function shape($w, $l) {
        $this->length = $l;
        $this->width = $w;
        $this->id = uniqid();
        
    }

    public function getname() {
        return $this->name;
    }

    function setname($name) {
        $this->name = $name;
    }

    public function getid() {
        return $this->id;
    }

    function area() {

        $area = $this->length * $this->width;
        return $area;
    }

    function getTypeDescription() {
        echo 'Type:' . self::shape_type ;
    }

    function getFullDescription() {
        echo 'Shape<' . $this->id . '>: ' . $this->name  .  ' - ' . $this->length . ' x ' . $this->width  ;
    }

}

