<?php

class Page
{
    public $pageName;

    public function shape()
    {
        require_once 'pages/shape.php';
    }
     public function rectangle()
    {
        require_once 'pages/rectangle.php';
    }
     public function circle()
    {
        require_once 'pages/circle.php';
    }
    public function __call($name, $arguments)
    {
        require_once 'pages/404.view.php';
    }
}