<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

        <?php
        require_once 'app/libs/functions.php';
        require_once 'app/model/shape.class.php';
        //require_once 'app/model/Pages.class.php';
        require_once 'app/model/rectangle.class.php';
        require_once 'app/model/circle.class.php';

        if (empty($_GET['p'])) {
        $page = 'index';
        } else {

        $page = $_GET['p'];
        }
        switch ($page) {
        case 'shape':
        require_once 'pages/shape.php';
        break;
        case 'rectangle':
        require_once 'pages/rectangle.php';
        break;
        case 'circle':
        require_once 'pages/circle.php';
        break;
        default:
        require_once 'pages/404.view.php';
        }

       