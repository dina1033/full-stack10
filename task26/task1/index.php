<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'app/libs/functions.php';
        require_once 'app/model/number.class.php';
        require_once 'app/model/Pages.class.php';
        require_once 'app/model/calculator.class.php';

        if (empty($_GET['p'])) {
            $page = 'index';
        } else {

            $page = $_GET['p'];
        }
        switch ($page) {
            case 'home':
                require_once 'pages/home.php';
                break;
            case 'calculator':
                 require_once 'pages/calculator.php';
                break;
            default:
                require_once 'pages/404.view.php';
        }
        ?>
    </body>
</html>
