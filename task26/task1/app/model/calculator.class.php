<?php

class calculator {

    public $number1;
    public $number2;

    function calculator($n1, $n2) {
        $this->number1 = $n1;
        $this->number2 = $n2;
    }

    function add() {
        $sum = $this->number1 + $this->number2;
        return $sum;
    }

    function subtract() {
        $subtract = $this->number1 - $this->number2;
        return $subtract;
    }

    function multiply() {
        $multiply = $this->number1 * $this->number2;
        return $multiply;
    }

    function divide() {
        $divide = $this->number1 / $this->number2;
        return $divide;
    }

}


