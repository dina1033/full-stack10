function employeeSalary(x) {
    let sal = x;
    return {
        bonus: function (a) {
            sal += a;
            return sal;
        },
        tax: function (b) {
            sal -= b;
            return sal;
        },
        totalSalary: function () {
            return sal;
        }
    };
}
const empSalary = employeeSalary(3000);
console.log(empSalary.bonus(150));
console.log(empSalary.tax(70));
console.log(empSalary.totalSalary());


