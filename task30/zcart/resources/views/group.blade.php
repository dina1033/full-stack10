<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/d876aa8ccf.js"></script>
    <title>Hello, world!</title>
  </head>
  <body>
 
  <div class="container bg-light">
    <div class="d-flex justify-content-between mt-5 mb-3 pt-2">
      <h5 >CATEGORY GROUPS</h5>
      <a href="{{route('group.create')}}" class="btn btn-dark">ADD CATEGORY GROUP</a>
    </div>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">group name</th>
          <th scope="col">group description</th>
        </tr>
      </thead>
      <tbody>
        @foreach($group as $item)
          <tr>
    
            <th scope="row">{{$item->id}}</th>
            <td>{{$item->name}}</td>
            <td>{{$item->description}}</td>
            <td>
              <a href="{{ route('group.edit', $item) }}" style="color:black "><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
  
              <form method="post"style="display: inline-block" action="{{ route('group.destroy', $item) }}">
                   @method('DELETE')
                   @csrf
                <button style="color:black ; border:none"><i class="fa fa-trash" aria-hidden="true"></i></button>
              </form>
     
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>


   
  </body>
</html>
















