<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/d876aa8ccf.js"></script>
    <title>add group</title>
  </head>
  <body>
  <section class="container">
    
      <a href="{{ route('group.index') }}" class="btn btn-info mt-5 mb-3">All groups</a>

      <form  method="post" action="{{isset($targetItem)? route('group.update', $targetItem) : route('group.store') }}">
        @csrf
        @isset ($targetItem)
            @method('PUT')
        @endif
        <div class="form-group">
            <label for="title">Group Name</label>
            <input type="text" id="title" name="name" class="form-control" value="{{ isset($targetItem)? $targetItem->name : '' }}">
        </div>
        <div class="form-group">
            <label for="body">Description</label>
            <textarea name="description" id="body" cols="30" rows="10" class="form-control" >{{ isset($targetItem)? $targetItem->description  : '' }}</textarea>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-success">{{ isset($targetItem)? 'Update' : 'Save' }}</button>
        </div>
      </form>
    
</section>
  </body>
</html>