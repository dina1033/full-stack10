<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/d876aa8ccf.js"></script>
    <title>add and update sub-group</title>
  </head>
  <body>
  <section class="container">
    <a href="{{ route('subgroup.index') }}" class="btn btn-info">All sub_groups</a>

    <form  method="post" action="{{isset($targetItem)? route('subgroup.update', $targetItem) : route('subgroup.store') }}">

        @csrf
        @isset ($targetItem)
            @method('PUT')
        @endif
        <div class="form-group">
            <label for="group">CATEGORY GROUP</label>
            <select name="groups_id" id="group" class="form-control">
                <option >{{ isset($targetItem)? $targetItem->groups_id :'select group name'}}</option>
                @foreach($group as $sub)
                    
                    <option value="{{ $sub->id }}"> {{ $sub->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="title">sub-group Name</label>
            <input type="text" id="title" name="name" class="form-control" value="{{ isset($targetItem)? $targetItem->name : '' }}">
        </div>
        <div class="form-group">
            <label for="body">Description</label>
            <textarea name="description" id="body" cols="30" rows="10" class="form-control" >{{ isset($targetItem)? $targetItem->description  : '' }}</textarea>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-success">{{ isset($targetItem)? 'Update' : 'Save' }}</button>
        </div>
    </form>
</section>
  </body>
</html>