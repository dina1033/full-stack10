<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subgroup extends Model
{
    protected $fillable=[
        'id',
        'name', 
        'description',
        'groups_id'
    ];

    
    public function group(){
        return $this->belongsTo(group::class);
    }

    public function category(){
        return $this->hasmany(category::class);
    }
}
