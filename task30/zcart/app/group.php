<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class group extends Model
{
    protected $fillable=[
        'id',
        'name', 
        'description'
    ];

    public function subgroup(){
        return $this->hasmany(supgroup::class);
    }
    
}
