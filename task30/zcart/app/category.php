<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $fillable=[
        'id',
        'name', 
        'description',
        'supgroups_id'
    ];


    public function group(){
        return $this->belongsTo(subgroup::class);
    }
}
