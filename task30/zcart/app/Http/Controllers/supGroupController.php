<?php

namespace App\Http\Controllers;
use App\subgroup;
use App\group;
use Illuminate\Http\Request;

class supGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subgroup=subgroup::all(['id','name' , 'description','groups_id']);
        return view('subgroup', compact('subgroup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group=group::all();
        return view('addsubgroup', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        subgroup::create($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(subgroup $subgroup)
    {
        return view('addsubgroup', [
            'targetItem' => $subgroup,
            'subgroup' => subgroup::all(['id', 'name', 'description' ]),
            'group'=>group::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subgroup $subgroup)
    {
        $subgroup->update($request->all());
        return redirect(route('subgroup.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(subgroup $subgroup)
    {
        $subgroup->delete();
         return redirect(route('subgroup.index'));
    }
}
