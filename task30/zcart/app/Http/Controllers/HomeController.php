<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
        return view('group');
        return view('subgroup');
        return view('category');
    }
    public function group()
    {
        
        return view('group');
        
    }
    public function subgroup()
    {
       
        return view('subgroup');
        
    }
    public function category()
    {
       
        return view('category');
    }
}
