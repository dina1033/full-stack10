<html>
    <header>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </header>
    <body>
        <div class="container mt-5">
            <table class="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Password</th>
                        <th scope="col">phone</th>
                        <th scope="col">date</th>
                        <th scope="col">date & time</th>
                        <th scope="col">Start month</th>
                        <th scope="col">number</th>
                        <th scope="col">Select:</th>
                        <th scope="col">range</th>
                        <th scope="col">search</th>
                        <th scope="col">time</th>
                        <th scope="col">url</th>
                        <th scope="col">week</th>
                        <th scope="col">img</th>

                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <th scope="row"><?php
                            $name = $_POST["name"];
                            echo $name;
                            ?></th>

                        <td><?php
                            $email = $_POST["email"];
                            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                echo $email;
                            } else {
                                echo $email . 'not valied';
                            }
                            ?></td>
                        <td><?php
                            $password = $_POST["password"];
                            echo $password;
                            ?></td>
                        <td><?php
                            $phone = $_POST["phone"];
                            echo $phone;
                            ?></td>
                        <td><?php
                            $date = $_POST["date"];
                            echo $date;
                            ?></td>
                        <td><?php
                            $datetime = $_POST["datetime"];
                            echo $datetime;
                            ?></td>
                        <td><?php
                            $month = $_POST["month"];
                            echo $month;
                            ?></td>
                        <td><?php
                            $number = $_POST["number"];
                            echo $number;
                            ?></td>
                        <td><?php
                            $number = $_POST["drone"];
                            echo $number;
                            ?></td>
                        <td><?php
                            $range = $_POST["range"];
                            echo $range;
                            ?></td>
                        <td><?php
                            $search = $_POST["search"];
                            echo $search;
                            ?></td>
                        <td><?php
                            $time = $_POST["time"];
                            echo $time;
                            ?></td>
                        <td><?php
                            $url = $_POST["url"];
                            echo $url;
                            ?></td>
                        <td><?php
                            $week = $_POST["week"];
                            echo $week;
                            ?></td>

                        <td><?php
                            
                            $img = $_FILES['img']['tmp_name'];
                            $tmp=file_get_contents($img);
                            echo ' <img src="data:image/jpeg;base64,' . base64_encode($tmp) . ' " width ="250" height="250">';
                            ?></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </body>
</html>