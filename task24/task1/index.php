<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <title></title>
        <style>
            label {
display: block;
                font: 1rem 'Fira Sans', sans-serif;
            }

            input,
            p,
            label {
                margin: .4rem 0;
                margin-left: 45px;

            }
            form{
                margin: auto;
    width: 400px;
    //height:560px;
    background: rgba(0, 0, 0, 0.5);
    box-sizing: border-box;
    border-radius: 35px;
    padding-top: 5px;

            }
        </style>
    </head>
    <body>
        <h2 class="text-center mt-3 mb-5">All types of form</h2>
        <form method="post" action="data.php" enctype="multipart/form-data">
            <label for="name">Name </label>
            <input type="text" id="name" name="name" required minlength="4" maxlength="8" size="30">
            <hr>
            <label>phone:</label>
            <input type="tel" name="phone"  required size="30">
            <hr>
            <label>email:</label>
            <input type="email" name="email"  pattern=".+@yahoo.com" size="30" required >
            <hr>
            <label>password:</label>
            <input type="password" name="password"  minlength="8" size="30">
            <hr>
            <label>date:</label>
            <input type="date" name="date"  > 
            <hr>
            <label>date & time:</label>
            <input type="datetime-local"  name="datetime">
            <hr>
            <label>Start month:</label>
            <input type="month" name="month" >
            <hr>
            <label>number:</label>
            <input type="number" name="number" >
            <hr>
            <p>Select:</p>
            <label for="huey">Huey</label>
            <input type="radio"  id="huey"  name="drone" value="huey" checked >
            <label for="dewey">Dewey</label>
            <input type="radio" id="dewey" name="drone" value="dewey">
            <label for="louie">Louie</label>
            <input type="radio" id="louie" name="drone" value="louie">
            <hr>
            <label>range:</label>
            <input type="range" min="0" max="11" name="range">
            <hr>
            <label>search:</label>
            <input type="search" aria-label="Search through site content" name="search">
            <hr>
            <label>Choose a time:</label>
            <input type="time"  min="09:00" max="18:00" required name="time">
            <hr>
            <label>Enter an https:// URL:</label>
            <input type="url"  placeholder="https://example.com" pattern="https://.*" size="30" required name="url">
            <hr>
            <label>Choose a week in May or June:</label>
            <input type="week" name="week" id="camp-week" min="2018-W18" max="2018-W26" required name="week">
            <hr>
            <input type="file" accept="image/png, image/jpeg" name="img">
            <hr>
            <input type="image" alt="Login" src="/media/examples/login-button.png" >
            <hr>
            <input type="submit" name="submit" value="submit"  class="ml-5 mr-5">
            <input type="reset" name="reset" value="reset" class="ml-5">
        </form>
    </body>
</html>
