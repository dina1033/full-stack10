import React, { Component } from 'react';
import './App.css';
import './index.scss';
import Navbar from "./Layouts/Navbar";
import Expenses from './component/expenses';
import List from './component/list';
import Addnew from './component/addnew';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {

  state = {
    items: [],
    itemname:'',
    itemprice:''
  };
  changehandelname = e => {
    const itemname = e.target.value;
    this.setState({
        itemname
    });
}

changehandelprice = e => {
    const itemprice = e.target.value;
    this.setState({
        itemprice
    });
}

  additem = () => {

    let items = this.state.items;
    const item = {
      id: this.state.items.length + 1,
      name: this.state.itemname,
      price: this.state.itemprice
    };
    items = [...items, item];

    this.setState({
      items,
      itemprice:'',
      itemname:''
    });

  };

  deletitem = (item) => {
    let items = this.state.items.filter(rowProject => item !== rowProject);
    this.setState({
      items
    });
  };


  render() {
    return (
      <div className="App">
        <Router>
          <Navbar />
          <div className="container">
            <Switch>
              <Route exact path="/">
                <Expenses items={this.state.items} />
              </Route>
              <Route exact path="/List">
                <List
                  items={this.state.items}
                  deletitem={this.deletitem} />
              </Route>
              <Route exact path="/Addnew">
                <Addnew additem={this.additem} 
                changehandelprice={this.changehandelprice}
                changehandelname={this.changehandelname}
                 />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    )
  }

}

export default App;
