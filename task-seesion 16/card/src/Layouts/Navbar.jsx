import React from 'react';
import { NavLink} from "react-router-dom";

const Navbar = () => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link ">
                        Expenses
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/List" className="nav-link">
                            List
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/AddNew" className="nav-link">
                          Add New
                        </NavLink>
                    </li>
                </ul>
            </div>
        
    </nav>
);

export default Navbar;