import React from 'react';
class Expenses extends React.Component {

    price = () => {
        let totalPrice = 0 ;
        this.props.items.map(item => (
            totalPrice += Number(item.price)
        ));

        return (totalPrice)
    }

    render() {
        return (
            <div className="container text-center">
                <h1 className="m-5">Expenses Calculator</h1>
                <h3 className="m-5">Your total experses are {(this.props.items.length) ? this.price() : 0} EGP</h3>
                <h3 className="m-5">You have {this.props.items.length} items in your exprnses list</h3>
            </div>
        );
    }

}


export default Expenses;