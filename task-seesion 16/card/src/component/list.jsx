import React from 'react';

export const List = props => (
    <div className="container">

        {
            (props.items.length) ?
                <div>
                    <h1 className="text-center mt-5 pb-5 ">Expenses List</h1>
                    <table className="table m-auto">
                    <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Item</th>
                                <th scope="col">Price</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                props.items.map(item => (
                                    <tr key={item.id}>
                                        <th>{item.id}</th>
                                        <td>{item.name}</td>
                                        <td>{Number(item.price)}</td>
                                        <td><button className="btn btn-sm btn-danger" onClick={() => props.deletitem(item)}>Delate</button></td>
                                    </tr>
                                ))
                            }
                        </tbody>


                    </table>
                </div>
                : <h1 className="text-center m-5 ">You haven't added any expenses yet</h1>
        }

    </div>
);

export default List;